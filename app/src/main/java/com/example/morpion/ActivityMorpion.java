package com.example.morpion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityMorpion extends AppCompatActivity implements View.OnClickListener {

    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btn_return;
    private TextView txtVictory;
    private ArrayList<Integer> JoueurUn;
    private ArrayList<Integer> JoueurDeux;
    int turn;
    int playerOne;
    int playerTwo;
    private int appTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_morpion);
        playerOne = R.drawable.ic_action_name;
        playerTwo = R.drawable.round;

        JoueurDeux = new ArrayList<Integer>();
        JoueurUn = new ArrayList<Integer>();
        for(int i = 0; i < 10; i++){
            JoueurUn.add(0);
            JoueurDeux.add(0);
        }

        turn = 0;
        txtVictory = (TextView) this.findViewById(R.id.txt_victory);
        btn_return = (Button) this.findViewById(R.id.btn_return);
        btn1 = (Button) this.findViewById(R.id.btn1);
        btn2 = (Button) this.findViewById(R.id.btn2);
        btn3 = (Button) this.findViewById(R.id.btn3);
        btn4 = (Button) this.findViewById(R.id.btn4);
        btn5 = (Button) this.findViewById(R.id.btn5);
        btn6 = (Button) this.findViewById(R.id.btn6);
        btn7 = (Button) this.findViewById(R.id.btn7);
        btn8 = (Button) this.findViewById(R.id.btn8);
        btn9 = (Button) this.findViewById(R.id.btn9);

        btn1.setBackground(null);
        btn2.setBackground(null);
        btn3.setBackground(null);
        btn4.setBackground(null);
        btn5.setBackground(null);
        btn6.setBackground(null);
        btn7.setBackground(null);
        btn8.setBackground(null);
        btn9.setBackground(null);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v == btn1){
            setbackground(v);
        }else if(v == btn2){
            setbackground(v);
        }else if(v == btn3){
            setbackground(v);
        }else if(v == btn4){
            setbackground(v);
        }else if(v == btn5){
            setbackground(v);
        }else if(v == btn6){
            setbackground(v);
        }else if(v == btn7){
            setbackground(v);
        }else if(v == btn8){
            setbackground(v);
        }else if(v == btn9){
            setbackground(v);
        }else if(v == btn_return){
            this.finish();
        }

        checkVictory();

        turn ++;
    }

    private void checkVictory() {
        if(JoueurUn.get(1) == 1 && JoueurUn.get(2) == 1 && JoueurUn.get(3) == 1 ||
            JoueurUn.get(4) == 1 && JoueurUn.get(5) == 1 && JoueurUn.get(6) == 1 ||
            JoueurUn.get(7) == 1 && JoueurUn.get(8) == 1 && JoueurUn.get(9) == 1 ||
                JoueurUn.get(1) == 1 && JoueurUn.get(4) == 1 && JoueurUn.get(7) == 1 ||
                JoueurUn.get(2) == 1 && JoueurUn.get(5) == 1 && JoueurUn.get(8) == 1 ||
                JoueurUn.get(3) == 1 && JoueurUn.get(6) == 1 && JoueurUn.get(9) == 1 ||
                JoueurUn.get(1) == 1 && JoueurUn.get(5) == 1 && JoueurUn.get(9) == 1 ||
                JoueurUn.get(3) == 1 && JoueurUn.get(5) == 1 && JoueurUn.get(7) == 1){
            setPanelVictory(1);
        }else if(JoueurDeux.get(1) == 1 && JoueurDeux.get(2) == 1 && JoueurDeux.get(3) == 1 ||
                JoueurDeux.get(4) == 1 && JoueurDeux.get(5) == 1 && JoueurDeux.get(6) == 1 ||
                JoueurDeux.get(7) == 1 && JoueurDeux.get(8) == 1 && JoueurDeux.get(9) == 1 ||
                JoueurDeux.get(1) == 1 && JoueurDeux.get(4) == 1 && JoueurDeux.get(7) == 1 ||
                JoueurDeux.get(2) == 1 && JoueurDeux.get(5) == 1 && JoueurDeux.get(8) == 1 ||
                JoueurDeux.get(3) == 1 && JoueurDeux.get(6) == 1 && JoueurDeux.get(9) == 1 ||
                JoueurDeux.get(1) == 1 && JoueurDeux.get(5) == 1 && JoueurDeux.get(9) == 1 ||
                JoueurDeux.get(3) == 1 && JoueurDeux.get(5) == 1 && JoueurDeux.get(7) == 1){
            setPanelVictory(1);
        }else if(turn == 8){
            setPanelVictory(0);
        }
    }

    private void setPanelVictory(int victory) {
        String txt;
        if(turn % 2 == 1 && victory == 1){
            txt = "Félicitation les ❌ ont gagné ! \uD83C\uDF89";
            showAddItemDialog(this, "Victoire !", txt );
        }else if(victory == 1){
            txt = "Félicitation les ⚪ ont gagné ! \uD83C\uDF89";
            showAddItemDialog(this, "Victoire !", txt );
        }
        if(turn == 8 && victory == 0){
            txt = "Vous avez fait égalité ! Sans doute le moment de faire une revanche !";
            showAddItemDialog(this, "Églité !", txt );
        }


    }


    private void setbackground(View v) {
        if(turn % 2 == 1){
            v.setBackgroundResource(playerOne);
            positionJoueur(1, v);
        }else {
            v.setBackgroundResource(playerTwo);
            positionJoueur(2, v);
        }
        v.setClickable(false);
    }

    private void positionJoueur(int i, View v) {
        switch (v.getId()) {
            case R.id.btn1:

                if(i == 1){
                    JoueurUn.set(1, 1);
                }else{
                    JoueurDeux.set(1, 1);
                }
                break;
            case R.id.btn2:
                if(i == 1){
                    JoueurUn.set(2, 1);
                }else{
                    JoueurDeux.set(2, 1);
                }
                break;
            case R.id.btn3:
                if(i == 1){
                    JoueurUn.set(3, 1);
                }else{
                    JoueurDeux.set(3, 1);
                }
                break;
            case R.id.btn4:
                if(i == 1){
                    JoueurUn.set(4, 1);
                }else{
                    JoueurDeux.set(4, 1);
                }
                break;
            case R.id.btn5:
                if(i == 1){
                    JoueurUn.set(5, 1);
                }else{
                    JoueurDeux.set(5, 1);
                }
                break;
            case R.id.btn6:
                if(i == 1){
                    JoueurUn.set(6, 1);
                }else{
                    JoueurDeux.set(6, 1);
                }
                break;
            case R.id.btn7:
                if(i == 1){
                    JoueurUn.set(7, 1);
                }else{
                    JoueurDeux.set(7, 1);
                }
                break;
            case R.id.btn8:
                if(i == 1){
                    JoueurUn.set(8, 1);
                }else{
                    JoueurDeux.set(8, 1);
                }
                break;
            case R.id.btn9:
                if(i == 1){
                    JoueurUn.set(9, 1);
                }else{
                    JoueurDeux.set(9, 1);
                }
                break;
        }
    }

    private void showAddItemDialog(Context c,String Title, String text) {
        final EditText taskEditText = new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle(Title)
                .setMessage(text)
                .setView(taskEditText)
                .setPositiveButton("Parfait !", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       finish();
                    }
                })
                .create();
        dialog.show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
