package com.example.morpion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Random;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends AppCompatActivity {

    private Button m_btn_solo;
    private Button m_btn_versus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        m_btn_solo = (Button)this.findViewById(R.id.btn_SoloGame);
        m_btn_versus = (Button)this.findViewById(R.id.btn_NormalGame);


        m_btn_solo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random rand = new Random();
                int n = rand.nextInt(2);
                if(n==1){
                    Toast.makeText(MainActivity.this, "GLaDOS n'est pas encore au point pour vous battre. ", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "Cake is a lie. ", Toast.LENGTH_SHORT).show();
                }

            }
        });

        m_btn_versus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openActivityMorpion();
            }
        });
    }

    private void openActivityMorpion() {
        Intent intent = new Intent(this, ActivityMorpion.class);
        startActivity(intent);
    }

}
